(ns worklog.jira
  (:require [clj-http.client :as client]
            [clojure.string  :as str]))


(defn make-resource-resolver [domain]
  (fn [resource]
    (str "https://" domain "/rest/api/3" resource)))


;; AK9m0cYOxqVfP9NPGtZxBA43
(defn get-current-user [email token resolver]
  (let [url (resolver "/myself")]
    (:body (client/get url
                       {:accept     :application/json
                        :basic-auth [email token]
                        :as         :json}))))

;; from and to are strings YYYY-MM-DD
(defn make-worklog-query [from to author]
  (str "worklogDate >= \"" from   "\" and "
       "worklogDate <= \"" to     "\" and "
       "worklogAuthor = "  author))


(defn make-worklog-query-params [jql]
  {:jql    jql
   :fields "timespent,issuetype,project,parent,summary,created,updated,worklog"})


(defn ^:private get-issues [response]
  (get-in response [:body :issues]))


(defn ^:private extract-text-from-comment [comment]
  (keep :text (tree-seq #(or (map? %) (vector? %)) identity comment)))


(defn ^:private map-comment [comment]
  (str/join "\n" (extract-text-from-comment comment)))


(defn ^:private map-worklog [worklog]
  (mapv
   (fn [w] {:time-spent         (:timeSpent        w)
            :time-spent-seconds (:timeSpentSeconds w)
            :started            (:started          w)
            :updated            (:updated          w)
            :created            (:created          w)
            :comment            (map-comment       (:comment w))})
   (:worklogs worklog)))


(defn ^:private map-parent [parent]
  {:key     (:key parent)
   :summary (get-in parent [:fields :summary])})


(defn ^:private map-project [project]
  (select-keys project #{:key :name}))


(defn ^:private map-issue [issue]
  (let [fields (:fields issue)]
    {:key        (:key        issue)
     :summary    (:summary    fields)
     :time-spent (:timespent  fields)
     :parent     (map-parent  (:parent  fields))
     :project    (map-project (:project fields))
     :worklog    (map-worklog (:worklog fields))}))


(defn ^:private map-issues [issues]
  (mapv map-issue issues))


(defn get-worklog [email token resolver from to author]
  (let [url (resolver "/search")]
    (-> (client/get url
                    {:accept       :application/json
                     :basic-auth   [email token]
                     :as           :json
                     :query-params (make-worklog-query-params
                                    (make-worklog-query from to author))})
        (get-in [:body :issues])
        (map-issues))))


(defn make-worklog-report [worklog]
  (group-by (juxt :project :parent) worklog))

;;(jira/get-worklog email token resolve-resource "2019-05-06" "2019-05-18" "vkurovsk")
