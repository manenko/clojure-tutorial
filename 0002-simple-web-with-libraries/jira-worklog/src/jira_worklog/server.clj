(ns jira-worklog.server
  (:require [immutant.web              :as web]
            [jira-worklog.infra.config :as config]
            [taoensso.timbre           :as timbre]
            [clojure.tools.cli         :as cli])
  (:gen-class))


(defn app [request]
  {:status 200
   :body   "Hello, world!"})


(def cli-options
  [["-p" "--profile PROFILE" "Profile name (dev, prod, or test)"
    :parse-fn #(keyword %)
    :default  :dev]])


(defn -main
  [& args]
  (let [conf (-> args
                 (cli/parse-opts cli-options)
                 (get-in [:options :profile])
                 config/read-config)]
    (timbre/merge-config! (get-in conf [:app :logging]))
    (let [server-conf (get-in conf [:app :server])
          server-conf (web/run app server-conf)]
      (timbre/debug "Debug message")
      (timbre/info "Starting HTTP server on port" (:port server-conf)))))
