(ns jira-worklog.infra.config
  (:require [aero.core       :as aero]
            [clojure.java.io :as io]))


(defn read-config
  ([profile]
   (aero/read-config (io/resource "config.edn")
                     {:profile profile}))
  ([]
   (read-config :dev)))
